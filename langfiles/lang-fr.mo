��          �            h     i     |     �     �  9   �  +   �  ,        L  
   Q     \     h  /   t  )   �  <   �  ;       G     \     y  $   �  J   �  ,   �  6   '     ^     j     y     �  /   �  8   �  @   �                      
       	                                                   CAS authentication Create a user account Deny access Failed authenticate on CAS Failed to create a user account based on your CAS user ID If the user ID does not exists in ovidentia No nickname associated to CAS authentication Save Server URI Server host Server port The CAS server configuration has not been found You are not allowed to login on this site Your nickname %s does not exists in Ovidentia users database Project-Id-Version: AuthCas
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-28 14:41+0100
PO-Revision-Date: 2014-11-28 14:41+0100
Last-Translator: de Rosanbo Paul <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: AuthCas_translate;AuthCas_translate:1,2
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: programs
 Authentification CAS Créer un compte utilisateur Refuser l'accès Échec de l'authentification sur CAS Echec de la création du compte utilisateur à partir de l'identifiant CAS Si l'utilisateur n'existe pas dans ovidentia Aucun identifiant associé à l’authentification CAS Enregistrer URL du serveur Nom d'hôte Port Le serveur CAS configuré n'a pas été trouvé Vous n'êtes pas autorisé à vous connecté sur ce site Votre identifiant %s n'existe pas dans la liste des utilisateurs 