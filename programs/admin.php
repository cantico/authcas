<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';



class AuthCas_ConfigurationPage
{


	private function getForm()
	{
		$W = bab_Widgets();
		$form = $W->Form();
		$form->setName('configuration')
			->addClass('BabLoginMenuBackground')
			->addClass('authcas-form')
			->addClass('widget-bordered')
			->addClass('widget-centered');
		
		$form->setCanvasOptions($form->Options()->width(70,'em'));
		
		$form->setHiddenValue('tg', bab_rp('tg'));
		$form->colon();

		$form->getLayout()->setVerticalSpacing(1,'em');
		
		
		
		$label = $W->Label(AuthCas_translate('Server host'));
		$input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('server_hostname');
		
		$form->addItem($W->VBoxItems($label, $input));
		
		$label = $W->Label(AuthCas_translate('Server port'));
		$input = $W->LineEdit()->setAssociatedLabel($label)->setSize(4)->setName('server_port');
		
		$form->addItem($W->VBoxItems($label, $input));
		
		$label = $W->Label(AuthCas_translate('Server URI'));
		$input = $W->LineEdit()->setAssociatedLabel($label)->setSize(60)->setName('server_uri');
		
		$form->addItem($W->VBoxItems($label, $input));
		
		$label = $W->Label(AuthCas_translate('If the user ID does not exists in ovidentia'));
		$input = $W->Select()
			->addOption(0, AuthCas_translate('Deny access'))
			->addOption(1, AuthCas_translate('Create a user account'))
			->setAssociatedLabel($label)->setName('user_create');
		
		$form->addItem($W->Items($label, $input));
		
		
		$form->addItem(
				$W->SubmitButton()
				->setLabel(AuthCas_translate('Save'))
		);

		$registry = AuthCas_registry();

		$server_hostname 			= $registry->getValue('server_hostname');
		$server_port				= $registry->getValue('server_port');
		$server_uri					= $registry->getValue('server_uri');
		$user_create				= $registry->getValue('user_create');

		$form->setValues(
			array(
				'configuration' => array(
					'server_hostname'		=> $server_hostname,
					'server_port' 			=> $server_port,
					'server_uri' 			=> $server_uri,
					'user_create'			=> $user_create
				)
			)
		);

		return $form;
	}




	public function display()
	{
		$W = bab_Widgets();
		$page = $W->BabPage();
		

		$page->addItem($this->getForm());
		$page->displayHtml();
	}


	public function save($configuration)
	{
		$registry = AuthCas_registry();

		$registry->setKeyValue('server_hostname'	, $configuration['server_hostname']);
		$registry->setKeyValue('server_port'		, (int) $configuration['server_port']);
		$registry->setKeyValue('server_uri'			, $configuration['server_uri']);
		$registry->setKeyValue('user_create'		, (int) $configuration['user_create']);
		
		bab_url::get_request('tg')->location();
	}
}


if (!bab_isUserAdministrator())
{
	return;
}


$page = new AuthCas_ConfigurationPage;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();
