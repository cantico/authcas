<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


bab_functionality::includeFile('PortalAuthentication');

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/CAS.php';
require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';



class Func_PortalAuthentication_AuthCas extends Func_PortalAuthentication
{

	public function getDescription() 
	{
		return AuthCas_translate("CAS authentication");
	}
	
	
	private function startClient()
	{
		// phpCAS::setDebug();
		
		$registry = AuthCas_registry();
		
		$server_hostname = $registry->getValue('server_hostname');
		$server_port = $registry->getValue('server_port');
		$server_uri = $registry->getValue('server_uri');
		
		if (empty($server_hostname) || empty($server_port))
		{
			$this->addError(AuthCas_translate('The CAS server configuration has not been found'));
			return false;
		}
		
		phpCAS::client(CAS_VERSION_2_0, $server_hostname, $server_port, $server_uri,false);
		
		// Pas de validation SSL pour le serveur CAS
		phpCAS::setNoCasServerValidation();
		
		return true;
	}

	private function authenticate()
	{
		if (!$this->startClient()) {
		    return false;
		}
		
		// Force l'authentification avec le serveur CAS
		return phpCAS::forceAuthentication();
	}

	public function login()
	{
		if ($this->isLogged())
		{
			return true;
		}
		
		global $PHPCAS_CLIENT;
		
		
		if (!isset($PHPCAS_CLIENT) || !phpCAS::isAuthenticated()) {
			if (!$this->authenticate())
			{
				$this->addError(AuthCas_translate('Failed authenticate on CAS'));
				return false;
			}
		}
		
		
		$nickname = phpCAS::getUser();
		
		if (!$nickname)
		{
			$this->addError(AuthCas_translate('No nickname associated to CAS authentication'));
			return false;
		}
		
		$registry = AuthCas_registry();
		$user_create = $registry->getValue('user_create');

		if ($row = bab_getUserByNickname($nickname))
		{
			
			$id_user = $row['id'];

		} else if (1 === $user_create) {
			
			$id_user = $this->registerCASUser($nickname);
			
			if (!$id_user) {
				$this->addError(AuthCas_translate('Failed to create a user account based on your CAS user ID'));
				return false;
			}
			
		} else {
			$this->addError(sprintf(AuthCas_translate('Your nickname %s does not exists in Ovidentia users database'), $nickname));
			return false;
		}
		
		
		if (!$this->userCanLogin($id_user))
		{
			$this->addError(AuthCas_translate('You are not allowed to login on this site'));
			return false;
		}
		
    	$this->setUserSession($id_user, null);
		return true;
	}
	
	
	/**
	 * @return int
	 */
	private function registerCASUser($nickname)
	{
		$firstname 	= 'CAS';
		$lastname 	= $nickname;
		$email 		= '';
		$password 	= uniqid();
		$error 		= '';
		
		// try to use infos given by CAS
		
		if (phpCAS::hasAttributes())
		{
		
			if ($val = phpCAS::getAttribute('givenname')) {
				$firstname = $val;
			}
		
			if ($val = phpCAS::getAttribute('sn')) {
				$lastname = $val;
			}
		
			if ($val = phpCAS::getAttribute('email')) {
				$email = $val;
			}
		}
		
		
		
		$id_user = bab_registerUser($firstname, $lastname, '', $email, $nickname, $password, $password, 1, $error);
		
		if (!$id_user)
		{
			$this->addError($error);
		}
		
		return $id_user;
	}
	
	
	
	
	public function logout()
	{
		$this->startClient();
		phpCAS::logout(); 
	}
}
