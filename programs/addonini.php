; <?php/*
[general]
name						="AuthCas"
version						="0.4"
encoding					="ISO-8859-15"
mysql_character_set_database="latin1,utf8"
description					="CAS authentication."
description.fr				="Authentification CAS."
long_description.fr         ="README.md"
delete						=1
ov_version					="6.6.91"
php_version					="4.1.2"
addon_access_control		=0
author						="Cantico"
icon						="icon.png"
mod_curl					="Available"
configuration_page 			= "admin"
tags						="library,authentication"

[addons]

widgets						=">=1.0.22"

;*/?>