# CAS

Le Central Authentication Service (CAS) est un système d'authentification unique (SSO) pour le web développé par l'Université Yale, partenaire majeur dans le développement de uPortal. Ce logiciel est implanté dans plusieurs universités et organismes dans le monde.

CAS est un système d'authentification unique : on s'authentifie sur un site Web, et on est alors authentifié sur tous les sites Web qui utilisent le même serveur CAS. Il évite de s'authentifier à chaque fois qu'on accède à une application en mettant en place un système de ticket.

# Module AuthCas

Dans Ovidentia, il faut configurer la fonctionnalité par défaut de PortalAuthentication dans : Administration > Ajouter/supprimer des programmes > librairies > Administrer les librairies 
ou utiliser l'url ?tg=login&sAuthType=Cas pour tester.

Le module est configurable pour les paramètres suivants :

* Server host
* Server port
* Server URI